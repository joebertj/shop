<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	 $faker = \Faker\Factory::create();
	 foreach (range(1,10) as $index) {   
             DB::table('products')->insert([
                'sports' =>  $faker->randomElement(['biking','football','running','swimming','yoga']),
                'description' => $faker->catchPhrase,
                'brand' => $faker->company,
                'price' => $faker->randomFloat(2,0.01,999999.99)
    	     ]);
	 }
    }
}
