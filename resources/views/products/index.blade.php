@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
  <h1 class="display-3">Product</h1>    
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}
    </div>
  @endif
</div>
<div class="col-sm-12">
  <a style="margin: 19px;" href="{{ route('products.create')}}" class="btn btn-primary">New product</a>
</div>
<div class="col-sm-12">
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Sports</td>
          <td>Description</td>
          <td>Brand</td>
          <td>Price</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($products as $product)
        <tr>
            <td>{{ $product->id }}</td>
            <td>{{ $product->sports }}</td>
            <td>{{ $product->description }}</td>
            <td>{{ $product->brand }}</td>
            <td>{{ $product->price }}</td>
            <td>
                <a href="{{ route('products.edit',$product->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('products.destroy', $product->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
</div>
</div>
@endsection
