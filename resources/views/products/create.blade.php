@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a product</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('products.store') }}">
          @csrf
          <div class="form-group">    
	      <label for="sports">Sports</label>
	      <select class="form-control" name="sports">
  		<option value="biking">Biking</option>
  		<option value="football">Football</option>
  		<option value="running">Running</option>
  		<option value="swimming">Swimming</option>
  		<option value="yoga">Yoga</option>
	      </select>
          </div>

          <div class="form-group">
              <label for="description">Description</label>
              <input type="text" class="form-control" name="description"/>
          </div>

          <div class="form-group">
              <label for="brand">Brand</label>
              <input type="text" class="form-control" name="brand"/>
          </div>
          <div class="form-group">
              <label for="price">Price</label>
              <input type="text" class="form-control" name="price"/>
          </div>
          <button type="submit" class="btn btn-primary-outline">Add product</button>
      </form>
  </div>
</div>
</div>
@endsection
