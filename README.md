# shop
A laravel shop demo
![shop](shop.png "shop")

## UI
```
composer require laravel/ui --dev
php artisan ui vue
```

## Quickstart
`laravel new shop`

### Built-in server
`php artisan serve`

### Model
`php artisan make:model Product --migration`
Add following class definition on `app/Product.php`
```
protected $fillable = [
        'sports',
        'description',
        'brand',
        'price'
    ];
```

### Controller
Create resource `app/Http/Controllers/ProductsController.php` 
`php artisan make:controller ProductsController --resource`

### Database
```
php artisan migrate
php artisan make:seeder ProductSeeder
php artisan db:seed --class ProductSeeder
php artisan migrate:refresh --seed
php artisan tinker
```
Add line `Schema::drop('products');`


### Issues
Add following line on `boot()` function of `app/Providers/AppServiceProvider.php` 
`Schema::defaultStringLength(191);`
